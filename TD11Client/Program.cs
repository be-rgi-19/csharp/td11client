﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TD11Client
{
    class Program
    {
        static void Main(string[] args)
        {
            RefServHM.Temps t = new RefServHM.Temps();
            RefServHM.Temps res;
            t.H = 1;
            t.M = 40;
            try
            {
                RefServHM.ServHmClient client = new RefServHM.ServHmClient();

                Console.WriteLine(client.HmToMin(t));
                res = client.MintoHm(35);
                
                client.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            Console.WriteLine("{0}:{1}", res.H, res.M);
            Console.ReadKey();
        }
    }
}
